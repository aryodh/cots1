from flask import Flask, request, render_template, jsonify
import requests, json

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

# http://127.0.0.1:5000/convert/?from=usd&to=idr&total=1
# @app.route('/convert/')
# def convert():
#     inp = request.args.to_dict()
#     link = "https://api.exchangeratesapi.io/latest?base=" + inp['from'].upper()
#     data = requests.get(link).json()
#     res = float(inp['total']) * data['rates'][inp['to'].upper()] 
#     return jsonify(
#         currency = inp['to'].upper(), 
#         total = "%.2f" % res,
#         date = data['date']
#         )

@app.route('/calculate/')
def calculate():
    inp = request.args.to_dict()
    a = int(inp['a'])
    b = int(inp['b'])

    print(inp)
    # res = float(inp['total']) * data['rates'][inp['to'].upper()] 
    return jsonify(
        answer = str(a+b)
    )



if __name__ == '__main__':
    app.run()